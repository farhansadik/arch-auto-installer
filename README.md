# Arch Linux Auto Installer 

A automatic arch linux installation script. Specially it is created for Virtual Mechine. 

# UNDER DEVELOPMENT 
# BIOS WORKED WELL
# UEFI UNDERWAY....

## Understanding Configuration File 
Before try, you mast read `config.conf` file. Fill those option carefully, do not make any mistake. A mistake could make installation wrong or harm your system. So make sure what are you doing. _**It is recomended to try this script on `Virtual Machine` first**_. 

1. **BIOS Mode** 

If you want to install **BIOS** sysytem then fill `BIOS` on `BOOT_MODE`, and for **UEFI** fill `UEFI`. To check supported BIOS/UEFI machine `efivar -l`, if you've seen any error, then its **BIOS**. Otherwise its **UEFI**.

2. **Partition Table**

Available Options : 1. GPT 2.MBR </br>
It will format your selected partition. If you've **BIOS** system then mark it as `MBR`. And for **UEFI** it must be in `GPT`. 
To go manual partitioning and avoid any hussle then leave it as empty.

Sample </br>
```
PARTITION_TABLE=GPT 
PARTITION_LOCATION=/dev/sda
```

> To View Partition Information ~ `lsblk` or `fdisk -l`

3. **EFI Partition**

* Getting Permission : To use EFI partition mark `EFI_PARTITION` as `YES` and to ignore mark it as `NO`. To go manual partitioning and avoid any hussle then mark it as `NO` or leave it as empty.
* Parition Size : `EFI_PARTITION_SIZE=300` size of partition must be in Megabyte. **Minimum size of EFI partition is 300MB.**
* Partition Location : Fill location of efi partition. Make no mistake. For fresh installation, it should be `/dev/sda1`

> If the disk from which you want to boot **already has an EFI system partition**, do not create another one, but use the existing partition instead.
Sample: </br>
```
EFI_PARTITION=YES
EFI_PARTITION_SIZE=300
EFI_PARTITION_LOCATION=/dev/sda1
```

4. **Swap Partition**

Currenty **SWAP** partition is not available. You can setup swap manually.

5. **Root Partition** 

* Getting Permission : To use RPPT partition then mark `ROOT_PARTITION` as `YES` and to ignore mark it as `NO`. To go manual partitioning and avoid any hussle then mark it as `NO` or leave it as empty.
* Parition Size : `ROOT_PARTITION_SIZE=9000` size of partition must be in Megabyte. **Minimum size of ROOT partition is 8192MB OR 8GB.**
* Partition Location : Fill location of root partition. Make no mistake. For fresh installation, it should be `/dev/sda2`

Sample: 
_NOTE_: `REMAINING` option is currently not available.
```
ROOT_PARTITION=YES
ROOT_PARTITION_SIZE=9000
ROOT_PARTITION_LOCATION=/dev/sda2
```

6. **Installation Packages** 

Default packages are `base`, `base-devel`, `linux`, `linux-firmware`
Optional options by default marked as `vim`, `git` and `neoftech`. You can add any packages if you want. Make sure its available on pacman repository. To add `pacstrap[3]=your_package`

7. **Time ZOne**

Default timezone is `TIMEZONE=/usr/share/zoneinfo/Asia/Dhaka`. To change find your preffered timezone from `/usr/share/zoneinfo/`

8. **Hostname**

Default hostname given as `archlinux`. 

9. **Root Password**

Default **root password** given is `root`. And default **root username** is `root`

## Installation Process 
1. Sync and Install Packages

Make sure you've internet access.
```bash
pacman -Sy
pacman -S git
```

2. Refresh Pacman Key 

Refresh packman key to avoid PGP signature error.
```bash
pacman-key --refresh-key
```

3. Clone Project 
```bash
git clone https://gitlab.com/farhansadik/arch-auto-installer.git
```

> MAKE SURE YOU'VE CHECKED AND FILLED CURRECT INFORMATION STORED INTO CONFIGURATION FILE. 

4. Install (Step 1)
```bash
cd arch-auto-installer
./install
```

5. Install (Step 2)
```bash
cd /home/arch-auto-installer
./chroot
```

6. Finishing Installation 
```bash 
exit 
umount /mnt 

# if efi partition mounted
umount /mnt/boot/efi
reboot
```

After reboot, remove your installation media. If you're running virtiual machine, then remve disk-iso or boot as exiting os option from grub menu. 

### LOG 
If you've received any error, then look for log file. 
log file can be found in `/root/installer/` and `/home/installer`. File name is `debug.log`

### Tutorial Video

null 

### Farhan Sadik