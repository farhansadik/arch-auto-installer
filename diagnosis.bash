#!/bin/bash 

source config.conf

# DIAGNOSIS 
# check 

function nullChecker() {
	if [[ $var == '' ]]; then {
		echo $1 is null, please check configuration file
	}
	elif [[ $1 == $1 ]]; then {
		echo $1=$var
	}
	else echo failed to detect $1
	fi
}

function partitionTable() {

	# not used 

	if [[ $var == '' ]]; then {
		echo Partition Table will be skipped!
	}
	elif [[ $var == 'GPT' || $var == 'MBR' ]]; then {
		echo PARTITION_TABLE=$var
	}
	else echo Failed to detect partition table!; exit 0
	fi 
}

function partitionManager() {
	if [[ $var == '' ]]; then {
		echo $1 Partition will be skipped! 
	}
	elif [[ $var == 'YES' ]]; then {
		echo $1=YES; 

		# checking size 
		if [[ $var2 = '' ]]; then {
			echo $2 is null! 
		}

		elif [[ $1 == "ROOT_PARTITION" ]]; then {
			
			# checking minimum size 
			if [[ $var2 -gt 8192 || $var2 -eq 8192 ]]; then {
				
				# minimum size will be 8GB
				echo $2=$var2;

				if [[ $var3 == '' ]]; then {
					echo $3 is null
					exit 0
				}
				else echo $3=$var3
				fi 
			}
			elif [[ $var2 -lt 8192 ]]; then {
				echo Minimum Size of ROOT Partition is 8192!
				exit 0
			}
			fi 
		}

		# efi partition 
		elif [[ $1 == "EFI_PARTITION" ]]; then {
			
			# checking minimum size 
			if [[ $var2 -gt 300 || $var2 -eq 300 ]]; then {
				
				# minimum size will be 300
				echo $2=$var2;

				if [[ $var3 == '' ]]; then {
					echo $3 is null
					exit 0
				}
				else echo $3=$var3
				fi 
			}
			elif [[ $var2 -lt 300 ]]; then {
				echo Minimum Size of EFI Partition is 300!
				exit 0
			}
			fi 
		}
		
		else echo Failed to detect $2; exit 0
		fi
		
	}
	else echo Failed to detect $1; exit 0
	fi 
}

# checking partition table 
if var=$PARTITION_TABLE; then partitionTable; fi

# checking efi partiton (min 300)
echo 
if var=$EFI_PARTITION; var2=$EFI_PARTITION_SIZE; var3=$EFI_PARTITION_LOCATION; then { 
	partitionManager "EFI_PARTITION" "EFI_PARTITION_SIZE" "EFI_PARTITION_LOCATION"
} fi

# checking root partition (min 8192)
echo
if var=$ROOT_PARTITION; var2=$ROOT_PARTITION_SIZE; var3=$ROOT_PARTITION_LOCATION; then {
	partitionManager "ROOT_PARTITION" "ROOT_PARTITION_SIZE" "ROOT_PARTITION_LOCATION"
} fi 
# templete 
# if var=$; var2=$; var3=$; ehen partitionManager; fi 

echo 
echo Reading custom 'pacstrap' packages
echo Packages : ${pacstrap[@]}

echo # timezone 
if var=$TIMEZONE; then nullChecker "TIMEZONE"; fi

echo # hostname 
if var=$HOSTNAME; then nullChecker "HOSTNAME"; fi 

echo #root password 
if var=$ROOT_PASSWORD; then nullChecker "ROOT_PASSWORD"; fi 

echo
echo finishing...